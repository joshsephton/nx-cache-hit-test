package location

import (
	"math/rand"

	"github.com/dhconnelly/rtreego"
)

var tol = 0.00000000001

// Point is our representation of a lon, lat pair.
type Point struct {
	Lon float64
	Lat float64
}

// Bounds returns a bounding box around the point for the R-Tree.
func (p *Point) Bounds() *rtreego.Rect {
	tp := &rtreego.Point{p.Lon, p.Lat}
	return tp.ToRect(tol)
}

type distribution string

// Distribution constants which let the user choose how the coordinates
// are generated.
const (
	Uniform distribution = "uniform"
	Normal               = "normal"
)

// RandomCoordinates generate random coordinates, with
// longitude = 2.0 ± .25, latitude = 52.4 ± .15. It
// will have a resolution of 1m or 0.00001°.
//
// The given distribution will determine how the points
// are generated within the bounding box.
func RandomCoordinates(dist string) (lon float64, lat float64) {
	switch distribution(dist) {
	case Uniform:
		lon = 1.75 + (float64(rand.Intn(50000)) / 100000.0)
		lat = 52.3 + (float64(rand.Intn(30000)) / 100000.0)
	case Normal:
		lon = 2.0 + rand.NormFloat64()*0.25/4
		lat = 52.4 + rand.NormFloat64()*0.15/4
	default:
		panic("cannot generate coordinates with unknown distribution")
	}
	return lon, lat
}
