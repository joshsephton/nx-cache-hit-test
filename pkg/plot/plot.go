package plot

import (
	"fmt"
	"image/color"
	"log"
	"math"
	"os"

	"gitlab.com/joshsephton/nx-cache-hit-test/pkg/location"
	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/vg"
	"gonum.org/v1/plot/vg/draw"
	"gonum.org/v1/plot/vg/vgimg"
	"gonum.org/v1/plot/vg/vgsvg"
)

// Result holds a position and whether it was a hit or a miss.
type Result struct {
	Point *location.Point
	Hit   bool
}

type format string

// Format constants which let users specify how the plot should be saved.
const (
	SVG format = "svg"
	PNG        = "png"
)

// Draw plots the given results and saves them to a file.
// An optional Ticker can be provided if you want custom
// gridlines. If none is given, the default is used.
func Draw(name string, format format, results []*Result, bboxes BoundingBoxer, ticker plot.Ticker) {

	if len(results) == 0 {
		log.Println("not drawing plots: no points")
		return
	}

	hitData := make(plotter.XYs, 0, len(results))
	missData := make(plotter.XYs, 0, len(results))

	minLon := results[0].Point.Lon
	maxLon := results[0].Point.Lon
	minLat := results[0].Point.Lat
	maxLat := results[0].Point.Lat

	for _, result := range results {
		if result.Hit {
			hitData = append(hitData, plotter.XY{
				// X: math.Round(result.Point.Lon*1000)/1000 - 0.0001 + float64(rand.Intn(20))/100000.,
				// Y: math.Round(result.Point.Lat*1000)/1000 - 0.0001 + float64(rand.Intn(20))/100000.,
				X: result.Point.Lon,
				Y: result.Point.Lat,
			})
		} else {
			missData = append(missData, plotter.XY{
				// X: math.Round(result.Point.Lon*1000)/1000 - 0.0001 + float64(rand.Intn(20))/100000.,
				// Y: math.Round(result.Point.Lat*1000)/1000 - 0.0001 + float64(rand.Intn(20))/100000.,
				X: result.Point.Lon,
				Y: result.Point.Lat,
			})
		}
		if result.Point.Lon < minLon {
			minLon = result.Point.Lon
		}
		if result.Point.Lat < minLat {
			minLon = result.Point.Lon
		}
		if result.Point.Lon > maxLon {
			maxLon = result.Point.Lon
		}
		if result.Point.Lat > maxLat {
			maxLon = result.Point.Lon
		}
	}

	p, err := plot.New()
	if err != nil {
		panic(err)
	}

	p.X.Label.Text = "Longitude"
	p.Y.Label.Text = "Latitude"
	p.X.Min = minLon
	p.X.Max = maxLon
	p.Y.Min = minLat
	p.Y.Max = maxLat
	p.BackgroundColor = color.Transparent

	if ticker != nil {
		p.X.Tick.Marker = ticker
		p.Y.Tick.Marker = ticker
	}

	p.Add(&plotter.Grid{
		Vertical: draw.LineStyle{
			Color: color.Gray{200},
			Width: vg.Points(.5),
		},
		Horizontal: draw.LineStyle{
			Color: color.Gray{200},
			Width: vg.Points(.5),
		},
	})

	hits, err := plotter.NewScatter(hitData)
	if err != nil {
		panic(err)
	}
	hits.GlyphStyle.Color = color.RGBA{G: 255, B: 100, A: 255}
	hits.GlyphStyle.Radius = vg.Length(1)

	misses, err := plotter.NewScatter(missData)
	if err != nil {
		panic(err)
	}
	misses.GlyphStyle.Color = color.RGBA{R: 255, G: 100, A: 255}
	misses.GlyphStyle.Radius = vg.Length(1)

	p.Add(misses, hits)

	if bboxes != nil {
		treePlot := NewQuadtree(bboxes)
		treePlot.Color = color.RGBA{A: 153}
		p.Add(treePlot)
	}

	var c vg.CanvasWriterTo

	switch format {
	case SVG:
		c = vgsvg.NewWith(
			vgsvg.UseWH(5*vg.Inch, 5*vg.Inch),
		)
	case PNG:
		c = vgimg.PngCanvas{
			Canvas: vgimg.NewWith(
				vgimg.UseWH(5*vg.Inch, 5*vg.Inch),
				vgimg.UseBackgroundColor(color.Transparent),
			),
		}
	}

	p.Draw(draw.New(c))

	f, err := os.Create(name)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	_, err = c.WriteTo(f)
	if err != nil {
		panic(err)
	}

	err = f.Close()
	if err != nil {
		panic(err)
	}
}

// Ticker creates ticks for the plot every 0.001° (approx 100m).
type Ticker struct {
	Major float64
	Minor float64
}

// Ticks returns Ticks in a specified range
func (t Ticker) Ticks(min, max float64) []plot.Tick {

	if max <= min {
		panic("illegal range")
	}

	if t.Major == 0.0 {
		panic("illegal major tick")
	}

	if t.Minor == 0.0 {
		panic("illegal minor tick")
	}

	var ticks []plot.Tick

	current := math.Floor(min) + (t.Major / 2)
	for {
		if current > max {
			break
		}
		ticks = append(ticks, plot.Tick{Value: current, Label: fmt.Sprintf("%.3f", current)})
		current += t.Major
	}

	current = math.Floor(min)
	for {
		if current > max {
			break
		}
		ticks = append(ticks, plot.Tick{Value: current})
		current += t.Minor
	}

	return ticks
}
