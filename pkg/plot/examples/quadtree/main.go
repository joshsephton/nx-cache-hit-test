package main

import (
	"image/color"
	"os"

	"github.com/dhconnelly/rtreego"
	"gitlab.com/joshsephton/nx-cache-hit-test/pkg/location"
	qt "gitlab.com/joshsephton/nx-cache-hit-test/pkg/plot"

	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/vg"
	"gonum.org/v1/plot/vg/draw"
	"gonum.org/v1/plot/vg/vgimg"
)

func main() {

	busStops := 1000

	stops := make([]rtreego.Spatial, busStops, busStops)

	for i := 0; i < busStops; i++ {
		lon, lat := location.RandomCoordinates(location.Normal)
		p := &location.Point{
			Lon: lon,
			Lat: lat,
		}
		stops[i] = p
	}

	rt := rtreego.NewTree(2, 3, 8, stops...)

	bboxes := rt.GetAllBoundingBoxes()
	minXYMaxXYs := make(qt.BoundingBoxes, busStops, busStops)
	for i, bbox := range bboxes {
		minXYMaxXYs[i] = qt.BoundingBox{
			MinX: bbox.PointCoord(0),
			MinY: bbox.PointCoord(1),
			MaxX: bbox.PointCoord(0) + bbox.LengthsCoord(0),
			MaxY: bbox.PointCoord(1) + bbox.LengthsCoord(1),
		}
	}

	p, err := plot.New()
	if err != nil {
		panic(err)
	}

	p.X.Label.Text = "Longitude"
	p.Y.Label.Text = "Latitude"
	p.X.Min = 1.75
	p.X.Max = 2.25
	p.Y.Min = 52.2
	p.Y.Max = 52.6
	p.BackgroundColor = color.Transparent

	p.Add(&plotter.Grid{
		Vertical: draw.LineStyle{
			Color: color.Gray{200},
			Width: vg.Points(.5),
		},
		Horizontal: draw.LineStyle{
			Color: color.Gray{200},
			Width: vg.Points(.5),
		},
	})

	scatterData := make(plotter.XYs, 0, busStops)
	for _, stop := range stops {
		s := stop.(*location.Point)
		scatterData = append(scatterData, plotter.XY{
			X: s.Lon,
			Y: s.Lat,
		})
	}

	scatterPlot, err := plotter.NewScatter(scatterData)
	if err != nil {
		panic(err)
	}
	scatterPlot.GlyphStyle.Radius = vg.Length(1)

	treePlot := qt.NewQuadtree(minXYMaxXYs)
	treePlot.Color = color.RGBA{R: 255, A: 255}

	p.Add(scatterPlot, treePlot)

	c := vgimg.PngCanvas{
		Canvas: vgimg.NewWith(
			vgimg.UseWH(10*vg.Inch, 10*vg.Inch),
			vgimg.UseBackgroundColor(color.White),
		),
	}
	p.Draw(draw.New(c))

	f, err := os.Create("quadtree.png")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	_, err = c.WriteTo(f)
	if err != nil {
		panic(err)
	}

	err = f.Close()
	if err != nil {
		panic(err)
	}

}
