package plot

import (
	"image/color"

	"gonum.org/v1/plot"
	"gonum.org/v1/plot/vg"
	"gonum.org/v1/plot/vg/draw"
)

// Quadtree implements the gonum Plotter interface, drawing a
// quadtree plot for the given bounding boxes.
type Quadtree struct {
	BoundingBoxes BoundingBoxes

	// Color is the color of the rectangle outlines.
	Color color.Color
}

// NewQuadtree creates a new Quadtree plot plotter for
// the given data.
func NewQuadtree(data BoundingBoxer) *Quadtree {
	cpy := CopyBoundingBoxes(data)
	return &Quadtree{
		BoundingBoxes: cpy,
	}
}

// Plot implements the Plot method of the plot.Plotter interface.
func (q *Quadtree) Plot(c draw.Canvas, plt *plot.Plot) {
	trX, trY := plt.Transforms(&c)

	c.SetColor(q.Color)

	for _, d := range q.BoundingBoxes {

		startX := trX(d.MinX)
		startY := trY(d.MinY)

		endX := trX(d.MaxX)
		endY := trY(d.MaxY)

		var p vg.Path
		p.Move(vg.Point{X: startX, Y: startY})
		p.Line(vg.Point{X: endX, Y: startY})
		p.Line(vg.Point{X: endX, Y: endY})
		p.Line(vg.Point{X: startX, Y: endY})
		p.Close()

		c.Stroke(p)
	}
}

// BoundingBoxer wraps the Len and BoundingBox methods.
type BoundingBoxer interface {
	// Len returns the number of bounding boxes.
	Len() int

	// MinXYMaxXY returns the min X,Y and max X,Y values.
	MinXYMaxXY(int) (float64, float64, float64, float64)
}

// BoundingBox is a single bounding box with a min X,Y and max X,Y.
type BoundingBox struct {
	MinX, MinY, MaxX, MaxY float64
}

// BoundingBoxes implements the BoundingBoxer interface using a slice.
type BoundingBoxes []BoundingBox

// Len implements the Len method of the BoundingBoxer interface.
func (bb BoundingBoxes) Len() int {
	return len(bb)
}

// MinXYMaxXY implements the MinXYMaxXY method of the BoundingBoxer interface.
func (bb BoundingBoxes) MinXYMaxXY(i int) (float64, float64, float64, float64) {
	return bb[i].MinX, bb[i].MinY, bb[i].MaxX, bb[i].MaxY
}

// CopyBoundingBoxes copies a BoundingBoxer.
func CopyBoundingBoxes(data BoundingBoxer) BoundingBoxes {
	cpy := make(BoundingBoxes, data.Len())
	for i := range cpy {
		cpy[i].MinX, cpy[i].MinY, cpy[i].MaxX, cpy[i].MaxY = data.MinXYMaxXY(i)
	}
	return cpy
}
