package cache

// Cache is a very simple local in-memory cache. It doesn't store any
// data, so isn't actually very useful as a cache. However, it lets
// us test the cache hit ratio when modelling cache strategies.
//
// It is possible to use Cache to simulate various TTLs. As we expect
// it to be used in a non-realtime scenario, the TTL must be set with
// the simulated requests per second. The cache members are invalidated
// by removing any entry which was set more than (TTL * Req/s) requests
// ago. For example if we simulate 1M requests in a 1 hour period then
// our requestsPerSecond is 277.77778. With a TTL of 30s, we delete
// anything set more than 8,333 requests ago.
//
// Note, we assume requests are uniformly distributed in time.
//
// Cache is not thread safe.
type Cache struct {
	currentRequest    int
	requestsPerSecond int
	maxAge            int
	members           map[string]int
	setOrder          map[int]string
}

// New returns a new initialised cache.
func New(requestsPerSecond int) *Cache {
	return &Cache{
		currentRequest:    0,
		requestsPerSecond: requestsPerSecond,
		members:           make(map[string]int),
		setOrder:          make(map[int]string),
	}
}

// Get retrieves a member from the cache.
func (c *Cache) Get(key string) (bool, int) {
	setAt, ok := c.members[key]
	age := 0
	if ok {
		age = (c.currentRequest - setAt) / c.requestsPerSecond
	}
	return ok, age
}

// Set adds a member to the cache.
func (c *Cache) Set(key string) {
	c.members[key] = c.currentRequest
	c.setOrder[c.currentRequest] = key
}

// SetTTL sets the maxAge property on the cache and causes anything
// set more than maxAge requests ago to be deleted.
//
// TTL should be given in seconds.
func (c *Cache) SetTTL(ttl int) {
	c.maxAge = c.requestsPerSecond * ttl
}

// Reset sets the currentRequest back to zero. Useful when you seed
// the cache with data before starting the simulation.
func (c *Cache) Reset() {
	c.currentRequest = 0
}

// NextRequest ...
func (c *Cache) NextRequest() {
	c.currentRequest++
	c.invalidateMembers()
}

func (c *Cache) invalidateMembers() {
	if c.maxAge > 0 {
		if key, ok := c.setOrder[c.currentRequest-c.maxAge]; ok {
			delete(c.members, key)
		}
	}
}
