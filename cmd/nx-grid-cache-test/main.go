package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"math/rand"
	"os"
	"sync"

	"gitlab.com/joshsephton/nx-cache-hit-test/pkg/cache"
	"gitlab.com/joshsephton/nx-cache-hit-test/pkg/location"
	"gitlab.com/joshsephton/nx-cache-hit-test/pkg/plot"
)

// Test holds everything required to run multiple tests using
// different cache key strategies.
type Test struct {
	name   string
	getKey func(lon float64, lat float64) string
}

func main() {

	var distribution string
	var passengers int
	var timeframe int
	var ttl int

	flag.StringVar(
		&distribution,
		"dist",
		"uniform",
		"The distribution of randomly generated coordinates {uniform | normal}",
	)

	flag.IntVar(
		&passengers,
		"passengers",
		1000000,
		"The number of visitors to simulate",
	)

	flag.IntVar(
		&timeframe,
		"timeframe",
		3600,
		"The period of time in which the visitors arrive (seconds)",
	)

	flag.IntVar(
		&ttl,
		"ttl",
		30,
		"The ttl to use for the cache (seconds)",
	)

	flag.Parse()

	rand.Seed(100)

	tests := []*Test{
		&Test{
			name:   "naive key - with invalidation",
			getKey: naiveCacheKey,
		},
		&Test{
			name:   "10m grid key  - with invalidation",
			getKey: tenMetreGridCacheKey,
		},
		&Test{
			name:   "100m grid key - with invalidation",
			getKey: hundredMetreGridCacheKey,
		},
	}

	var wg sync.WaitGroup

	for _, test := range tests {

		wg.Add(1)

		go func(wg *sync.WaitGroup, test *Test) {

			defer wg.Done()

			passengersPerSecond := passengers / timeframe
			cache := cache.New(passengersPerSecond)
			cache.SetTTL(ttl)

			cacheHits := 0
			apiCalls := 0

			results := make([]*plot.Result, passengers, passengers)

			f, err := os.Create(fmt.Sprintf("output - %s - %s.csv", test.name, distribution))
			if err != nil {
				panic("couldn't create file")
			}
			defer f.Close()

			w := csv.NewWriter(f)
			defer w.Flush()

			for i := 0; i < passengers; i++ {

				cache.NextRequest()

				lon, lat := location.RandomCoordinates(distribution)

				key := test.getKey(lon, lat)
				hit, _ := cache.Get(key)
				if hit {
					cacheHits++
				} else {
					apiCalls++
					cache.Set(key)
				}

				r := &plot.Result{
					Point: &location.Point{
						Lon: lon,
						Lat: lat,
					},
					Hit: hit,
				}

				results[i] = r

				w.Write([]string{
					fmt.Sprintf("%.5f", r.Point.Lon),
					fmt.Sprintf("%.5f", r.Point.Lat),
					fmt.Sprintf("%t", r.Hit),
				})

			}

			cacheHitRatio := float64(cacheHits) / float64(passengers)
			msg := "%s cache hit: %d/%d = %.3f%%\n"
			fmt.Printf(msg, test.name, cacheHits, passengers, cacheHitRatio*100)

			msg = "%s api calls: %d\n"
			fmt.Printf(msg, test.name, apiCalls)

			plot.Draw(fmt.Sprintf("plot - %s - %s.png", test.name, distribution), plot.PNG, results, nil, nil)

			// Find a subset of the points which fall in the bounding box:
			//    (1.75, 52.3), (1.76, 52.31)
			zoomResults := make([]*plot.Result, 0, 0)
			for _, result := range results {
				if result.Point.Lon >= 2.00 && result.Point.Lon < 2.01 && result.Point.Lat >= 52.40 && result.Point.Lat < 52.41 {
					zoomResults = append(zoomResults, result)
				}
			}
			plot.Draw(fmt.Sprintf("zoom plot - %s - %s.svg", test.name, distribution), plot.SVG, zoomResults, nil, plot.Ticker{Major: 0.001, Minor: 0.0001})

		}(&wg, test)

	}

	wg.Wait()

}

func naiveCacheKey(lon float64, lat float64) string {
	return fmt.Sprintf("%.5f#%.5f", lon, lat)
}

func tenMetreGridCacheKey(lon float64, lat float64) string {
	return fmt.Sprintf("%.4f#%.4f", lon, lat)
}

func hundredMetreGridCacheKey(lon float64, lat float64) string {
	return fmt.Sprintf("%.3f#%.3f", lon, lat)
}
