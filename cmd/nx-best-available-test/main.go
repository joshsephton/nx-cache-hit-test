package main

import (
	"flag"
	"fmt"
	"math/rand"
	"sync"

	"github.com/dhconnelly/rtreego"

	"gitlab.com/joshsephton/nx-cache-hit-test/pkg/cache"
	"gitlab.com/joshsephton/nx-cache-hit-test/pkg/location"
)

// Test holds everything required to run multiple tests using
// different cache key strategies.
type Test struct {
	name   string
	getKey func(lon float64, lat float64) string
}

func main() {

	var distribution string
	var passengers int
	var busStops int
	var timeframe int
	var ratelimit int

	flag.StringVar(
		&distribution,
		"dist",
		"uniform",
		"The distribution of randomly generated coordinates {uniform | normal}",
	)

	flag.IntVar(
		&passengers,
		"passengers",
		1000000,
		"The number of visitors to simulate",
	)

	flag.IntVar(
		&busStops,
		"busstops",
		10000,
		"The number of bus stops to simulate",
	)

	flag.IntVar(
		&timeframe,
		"timeframe",
		3600,
		"The period of time in which the visitors arrive (seconds)",
	)

	flag.IntVar(
		&ratelimit,
		"ratelimit",
		10,
		"The rate at which requests for data can be made (per second)",
	)

	flag.Parse()

	rand.Seed(100)

	tests := []*Test{
		&Test{
			name:   "bus stop cache - best available",
			getKey: naiveCacheKey,
		},
	}

	var wg sync.WaitGroup

	for _, test := range tests {

		wg.Add(1)

		go func(wg *sync.WaitGroup, test *Test) {

			defer wg.Done()

			passengersPerSecond := passengers / timeframe
			cache := cache.New(passengersPerSecond)

			treeStops := make([]rtreego.Spatial, busStops, busStops)
			stops := make([]*location.Point, busStops, busStops)

			for i := 0; i < busStops; i++ {
				lon, lat := location.RandomCoordinates(location.Normal)
				p := &location.Point{
					Lon: lon,
					Lat: lat,
				}
				stops[i] = p
				treeStops[i] = p
				cache.Set(test.getKey(lon, lat))
				cache.Reset()
			}

			rt := rtreego.NewTree(2, 3, 8, treeStops...)

			rand.Seed(100)

			currentTime := 0
			currentRequest := 0
			requestsMadeThisSecond := 0
			totalAge := 0
			totalRequests := 0

			for i := 0; i < passengers; i++ {

				cache.NextRequest()

				nextTime := currentRequest / passengersPerSecond
				if nextTime > currentTime {
					requestsMadeThisSecond = 0
				}

				currentTime = nextTime
				hasAPIBudget := true
				if requestsMadeThisSecond >= ratelimit {
					hasAPIBudget = false
				}

				lon, lat := location.RandomCoordinates(distribution)

				// Find bus stops within a 100m radius.
				radius := 0.001
				bb, _ := rtreego.NewRect(rtreego.Point{lon - radius, lat - radius}, []float64{radius * 2, radius * 2})
				closestStops := rt.SearchIntersect(bb)

				if len(closestStops) == 0 {
					continue
				}

				for _, v := range closestStops {
					stop := v.(*location.Point)
					key := test.getKey(stop.Lon, stop.Lat)
					if !hasAPIBudget {
						_, age := cache.Get(key)
						totalAge = totalAge + age
						totalRequests++
					} else {
						cache.Set(key)
					}
				}

				currentRequest++
				requestsMadeThisSecond++

			}

			msg := "%s | average age: %.3fs\n"
			fmt.Printf(msg, test.name, float64(totalAge)/float64(totalRequests))

		}(&wg, test)

	}

	wg.Wait()

}

func naiveCacheKey(lon float64, lat float64) string {
	return fmt.Sprintf("%.5f#%.5f", lon, lat)
}
