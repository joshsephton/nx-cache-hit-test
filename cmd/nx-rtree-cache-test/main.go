package main

import (
	"flag"
	"fmt"
	"log"
	"math/rand"
	"sync"
	"time"

	"github.com/dhconnelly/rtreego"

	"gitlab.com/joshsephton/nx-cache-hit-test/pkg/cache"
	"gitlab.com/joshsephton/nx-cache-hit-test/pkg/location"
	"gitlab.com/joshsephton/nx-cache-hit-test/pkg/plot"
)

// Test holds everything required to run multiple tests using
// different cache key strategies.
type Test struct {
	name   string
	getKey func(lon float64, lat float64) string
}

func main() {

	var distribution string
	var passengers int
	var busStops int
	var timeframe int
	var ttl int

	flag.StringVar(
		&distribution,
		"dist",
		"uniform",
		"The distribution of randomly generated coordinates {uniform | normal}",
	)

	flag.IntVar(
		&passengers,
		"passengers",
		1000000,
		"The number of visitors to simulate",
	)

	flag.IntVar(
		&busStops,
		"busstops",
		10000,
		"The number of bus stops to simulate",
	)

	flag.IntVar(
		&timeframe,
		"timeframe",
		3600,
		"The period of time in which the visitors arrive (seconds)",
	)

	flag.IntVar(
		&ttl,
		"ttl",
		30,
		"The ttl to use for the cache (seconds)",
	)

	flag.Parse()

	rand.Seed(100)

	tests := []*Test{
		&Test{
			name:   "bus stop cache - with invalidation",
			getKey: naiveCacheKey,
		},
	}

	var wg sync.WaitGroup

	for _, test := range tests {

		wg.Add(1)

		go func(wg *sync.WaitGroup, test *Test) {

			defer wg.Done()

			treeStops := make([]rtreego.Spatial, busStops, busStops)
			stops := make([]*location.Point, busStops, busStops)

			for i := 0; i < busStops; i++ {
				lon, lat := location.RandomCoordinates(location.Normal)
				p := &location.Point{
					Lon: lon,
					Lat: lat,
				}
				stops[i] = p
				treeStops[i] = p
			}

			rt := rtreego.NewTree(2, 1, 4, treeStops...)

			log.Printf("built tree at %s", time.Now())

			passengersPerSecond := passengers / timeframe
			cache := cache.New(passengersPerSecond)
			cache.SetTTL(ttl)

			cacheHits := 0
			apiCalls := 0

			results := make([]*plot.Result, passengers, passengers)

			rand.Seed(100)

			requests := 0
			pointsWithoutStops := 0
			for i := 0; i < passengers; i++ {

				cache.NextRequest()

				lon, lat := location.RandomCoordinates(distribution)

				// Find bus stops within a 150m radius.
				radius := 0.0015
				bb, _ := rtreego.NewRect(rtreego.Point{lon - radius, lat - radius}, []float64{radius * 2, radius * 2})
				closestStops := rt.SearchIntersect(bb)

				if len(closestStops) == 0 {
					pointsWithoutStops++
					continue
				}

				requiredAPICall := false
				for _, v := range closestStops {
					stop := v.(*location.Point)
					key := test.getKey(stop.Lon, stop.Lat)
					requests++
					hit, _ := cache.Get(key)
					if !hit {
						requiredAPICall = true
						cache.Set(key)
					}
				}

				// We can get info for all the stops in this radius with one call
				if requiredAPICall {
					apiCalls++
				} else {
					cacheHits++
				}

				result := &plot.Result{
					Point: &location.Point{
						Lon: lon,
						Lat: lat,
					},
					Hit: !requiredAPICall,
				}

				results[i] = result

			}

			log.Printf("finished at %s", time.Now())

			bboxes := make(plot.BoundingBoxes, busStops, busStops)
			for i, bbox := range rt.GetAllBoundingBoxes() {
				bboxes[i] = plot.BoundingBox{
					MinX: bbox.PointCoord(1),
					MinY: bbox.PointCoord(0),
					MaxX: bbox.PointCoord(1) + bbox.LengthsCoord(1),
					MaxY: bbox.PointCoord(0) + bbox.LengthsCoord(0),
				}
			}

			plot.Draw(fmt.Sprintf("rtree - %s - %s.png", test.name, distribution), plot.PNG, results, bboxes, nil)

			cacheHitRatio := float64(cacheHits) / float64(passengers)
			msg := "%s | cache hit: %d/%d = %.3f%%\n"
			fmt.Printf(msg, test.name, cacheHits, passengers, cacheHitRatio*100)

			msg = "%s | api calls: %d\n"
			fmt.Printf(msg, test.name, apiCalls)

			msg = "%s | points without stops: %d\n"
			fmt.Printf(msg, test.name, pointsWithoutStops)

			log.Printf("created files at %s", time.Now())

		}(&wg, test)

	}

	wg.Wait()

}

func naiveCacheKey(lon float64, lat float64) string {
	return fmt.Sprintf("%.5f#%.5f", lon, lat)
}
